/**
 *  Run the Lambda function handler locally
 */
require('dotenv').config()
const index = require('./function/index');
const event = require('./data/event.json');
const { ReturnValue } = require('@aws-sdk/client-dynamodb');

(async () => {
    const response = await index.handler(event, {});
    console.log('## Response: ' + JSON.stringify(response, null, 2));
})();