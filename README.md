Node.JS Lambda function to sign x509 Certificate from S3 Bucket with generated RSA/ECC private key and store the signature in DynamoDB.

- Function entry point: ```function/index.js```

- CloudFormation template: ```template.yml```

- IAM Policy: ```iam_policy_template.json```

## Used Libraries

Node.JS Crypto module - [node:crypto](https://nodejs.org/api/crypto.html)

AWS SDK for JS v3:
 - [DynamoDB Client](https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/client/dynamodb/)
-  [S3 Client](https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/client/s3/)

## DEVELOPMENT

Execute the Lambda function locally by running:

```
npm run dev
```

### Prerequirements

In order to run locally, please setup test S3 Bucket, DynamoDB Table and provide their names in the local `.env`

.env file content:

```
TABLE_NAME=""
BUCKET_NAME=""
```

### Helper Scripts

Dependencies: [AWS CLI](https://aws.amazon.com/cli/)

Check if AWS CLI is configured correctly and has access to S3 Buckets

```
scripts/0-check-aws-cli.sh
```

1. Create S3 Bucket

```
scripts/1-create-bucket.sh
```

2. Generate x509 certificate and upload it to S3 Bucket

```
scripts/2-x509-generate-upload-s3.sh
```

3. Create DynamoDB Table

```
scripts/3-create-dynamodb.sh
```

4. Deploy to AWS using CloudFormation and the `template.yaml`
NOTE: for demonstration purposes only, not really tested.

```
scripts/4-deploy.sh
```

### Test cases

The test cases cover the end to end execution of the Lambda function and verifies the signature is valid against the generated key-pair

```
npm run test
```
