const certificate = require('./helpers/certificate');
const {
  generateRSAKeyPair, 
  signRSA,
  generateECCKeyPair,
  signECC
} = require('./helpers/encryption');
const { putCertificateSignature } = require('./helpers/database');

// Lambda Function Handler
exports.handler = async function (event, context) {
  console.log('# CERTIFICATE SIGN FUNCTION: ' + context.functionName);
  console.log('## TABLE_NAME: ' + process.env.TABLE_NAME,
        '## BUCKET_NAME: ' + process.env.BUCKET_NAME);

  const bucketName = process.env.BUCKET_NAME;

  const certificateFileName = event?.queryStringParameters?.certificate;
  console.log('## Certificate File: ' + JSON.stringify(certificateFileName));

  if (!certificateFileName) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        error: 'Certificate file is required'
      })
    };
  }

  //Read Certificate file from S3 bucket as X509Certificate object
  const x509Cert = await certificate.readX509Certificate(bucketName, certificateFileName);

  // Extract CommonName from the certificate
  const certCommonName = certificate.getCommonName(x509Cert);
  console.log('## Certificate CommonName: ' + certCommonName);

  // Extract Public Key from the certificate
  const certPublicKey = certificate.getPublicKey(x509Cert);
  console.log('## Certificate PublicKey: ' + certPublicKey);

  //Example of how to generate RSA Key Pair and sign the certificate's public key with the RSA private key:
  //
  //Generate RSA Key Pair
  // const {
  //   publicKey: generatedPublicKey,
  //   privateKey: generatedPrivateKey
  // } = generateRSAKeyPair();
  // console.log('## Generated PublicKey: ' + generatedPublicKey);

  // //Sign certificate's public key with the generated private key
  // const signature = signRSA(certPublicKey, generatedPrivateKey);
  // console.log('## Signature: ' + signature);

  //Generate ECC Key Pair
  const {
    publicKey: generatedPublicKey,
    privateKey: generatedPrivateKey
  } = generateECCKeyPair();
  console.log('## Generated PublicKey: ' + generatedPublicKey);

  //Sign certificate's public key with the generated private key
  const signature = signECC(certPublicKey, generatedPrivateKey);
  console.log('## Signature: ' + signature);

  //Write the certificate's Signature and the signature's public key to DynamoDB
  await putCertificateSignature(certCommonName, signature, generatedPublicKey);

  return {
    statusCode: 200,
    body: JSON.stringify({
      Certificate: {
        CommonName: certCommonName,
        PublicKey: certPublicKey,
      },
      PublicKey: generatedPublicKey,
      Signature: signature
    })
  };
}

