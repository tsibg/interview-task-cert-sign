const { S3Client, GetObjectCommand } = require("@aws-sdk/client-s3");
const { X509Certificate } = require('node:crypto');

const s3Client = new S3Client();

/**
 * Read file from S3 bucket
 *
 * @param {string} bucket
 * @param {string} file
 *
 * @returns {Promise<string>}   File content
 */
const readS3File = async function (bucket, file) {
    const command = new GetObjectCommand({
        Bucket: bucket,
        Key: file,
    });

    const response = await s3Client.send(command);
    const fileContent = await response.Body.transformToString();

    return fileContent;
}

/**
 * Load X.509 certificate from S3 bucket
 *
 * @param {string} bucket
 * @param {string} file
 *
 * @returns {X509Certificate}   X.509 certificate
 */
const readX509Certificate = async function (bucket, file) {
    const certContent = await readS3File(bucket, file);
    return new X509Certificate(certContent);
}

/**
 * Get Common Name from X509Certificate object
 *
 * @param {X509Certificate} x509Cert
 *
 * @returns {string}   Common Name
 */
const getCommonName = function (x509Cert) {
    return x509Cert.toLegacyObject().subject?.CN;
}

/**
 * Get Public Key from X509Certificate object
 *
 * @param {X509Certificate} x509Cert
 *
 * @returns {string}   Public Key
 */
const getPublicKey = function (x509Cert) {
    return x509Cert.publicKey.export({ type: 'spki', format: 'pem' });
}

module.exports = {
    readS3File,
    readX509Certificate,
    getCommonName,
    getPublicKey,
}
