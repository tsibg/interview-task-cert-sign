const { PutItemCommand, DynamoDBClient } = require("@aws-sdk/client-dynamodb");
const dbClient = new DynamoDBClient({});

/**
 * Write Certificate Signature to DynamoDB
 * 
 * @param {string} commonName    Certificate Common Name
 * @param {string} signature     Certificate Signature
 * @param {string} signaturePublicKey  Signature Public Key
 * 
 * @returns  {Promise}           DynamoDB command response   
 */
const putCertificateSignature = async (commonName, signature, signaturePublicKey) => {
    const command = new PutItemCommand({
        TableName: process.env.TABLE_NAME,
        Item: {
            CommonName: { S: commonName },
            Signature: { S: signature },
            PublicKey: { S: signaturePublicKey }
        },
    });

    const response = await dbClient.send(command);
    console.log("## Write Certificate SIgnature to DynamoDB", response);
    return response;
};

module.exports = { putCertificateSignature };