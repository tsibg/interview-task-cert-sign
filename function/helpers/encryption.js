const { generateKeyPairSync, createSign, createECDH } = require("node:crypto");

/**
 * Generate RSA Key Pair
 *
 * @returns {Object}   RSA Key Pair
 */
const generateRSAKeyPair = () => {
    //Generate RSA Key Pair
    const {
        publicKey,
        privateKey,
    } = generateKeyPairSync('rsa', {
        modulusLength: 4096,
        publicKeyEncoding: {
            type: 'spki',
            format: 'pem',
        },
        privateKeyEncoding: {
            type: 'pkcs8',
            format: 'pem',
        },
    });

    return {
        publicKey,
        privateKey
    };
};

/**
 * Sign data with RSA private key
 *
 * @param {string} data
 * @param {string} privateKey
 *
 * @returns {string}   Signature
 */
const signRSA = (data, privateKey) => {
    const sign = createSign('RSA-SHA256');
    sign.write(data);
    sign.end();
    return sign.sign(privateKey, 'base64');
};

/** Generate ECC private key
 * 
 * @returns {Object} ECC Key Pair
 */
const generateECCKeyPair = () => {
    // Creating ECDH with curve name 
    const { publicKey, privateKey } = generateKeyPairSync('ec', {
        namedCurve: 'secp256k1', 
        publicKeyEncoding: {
            type: 'spki',
            format: 'pem'
        },
        privateKeyEncoding: {
            type: 'pkcs8',
            format: 'pem'
        }
    });

    return {
        publicKey,
        privateKey
    };
}

/** Sign data with ECC private key 
 * @param {string} data
 * @param {string} privateKey
 * 
 * @returns {string} Signature
*/
const signECC = (data, privateKey) => {
    const sign = createSign('SHA256');
    sign.write(data);
    sign.end();
    return sign.sign(privateKey, 'base64');
}

module.exports = {
    generateRSAKeyPair,
    signRSA,
    generateECCKeyPair,
    signECC
}
