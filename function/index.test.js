require('dotenv').config();
const index = require('./index');
const { createVerify } = require('node:crypto');
const fs = require('fs');

const isJSON = (json) => {
  try {
    JSON.parse(json);
    return true;
  } catch (error) {
    return false;
  }
}

describe('Lambda function end-to-end test', () => {
  let body;

  test('Runs Lambda function handler', async () => {
    let eventFile = fs.readFileSync('data/event.json');
    let event = JSON.parse(eventFile);
    response = await index.handler(event, {});

    console.log('## Response: ' + JSON.stringify(response, null, 2));

    expect(response.statusCode).toBe(200);
    expect(response).toHaveProperty('body');
    expect(isJSON(response.body)).toBe(true);
  });

  test('Verifies function response data', async () => {
    body = JSON.parse(response.body);

    expect(body).toHaveProperty('Certificate');

    expect(typeof body.Certificate.CommonName).toBe('string');
    expect(body.Certificate.PublicKey).toContain('BEGIN PUBLIC KEY');

    expect(body.PublicKey).toContain('BEGIN PUBLIC KEY');;
    expect(typeof body.Signature).toBe('string');
  });

  test('Verifies the certificate signature', async () => {
    const certPublicKey = body.Certificate.PublicKey;
    const signature = body.Signature;
    const publicKey = body.PublicKey;

    const verify = createVerify('sha256');
    verify.write(certPublicKey);
    verify.end();
    const result = verify.verify(publicKey, signature, 'base64');
    expect(result).toBe(true);
  });
});
