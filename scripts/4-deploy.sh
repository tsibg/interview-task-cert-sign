#!/bin/bash
set -eo pipefail
ARTIFACT_BUCKET=$(cat data/1-bucket-name.txt)
aws cloudformation package --template-file template.yaml --output-template-file data/out.yml --s3-bucket $ARTIFACT_BUCKET
aws cloudformation deploy --region eu-central-1 --template-file data/out.yml --stack-name task-cert-sign --capabilities CAPABILITY_NAMED_IAM
