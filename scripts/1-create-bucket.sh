#!/bin/bash
set -eo pipefail
BUCKET_ID=$(dd if=/dev/random bs=8 count=1 2>/dev/null | od -An -tx1 | tr -d ' \t\n')
BUCKET_NAME=$BUCKET_ID-task-cert-sign
echo $BUCKET_NAME > data/1-bucket-name.txt
aws s3 mb s3://$BUCKET_NAME --region eu-central-1