#!/bin/bash
TABLE_ID=$(dd if=/dev/random bs=8 count=1 2>/dev/null | od -An -tx1 | tr -d ' \t\n')
TABLE_NAME=$TABLE_ID-CertificatesTable

# Create a DynamoDB table
aws dynamodb create-table --region eu-central-1 --table-name $TABLE_NAME \
 --attribute-definitions AttributeName=CommonName,AttributeType=S \
 --key-schema AttributeName=CommonName,KeyType=HASH \
 --provisioned-throughput ReadCapacityUnits=1,WriteCapacityUnits=1

# Describe the DynamoDB table
aws dynamodb describe-table --table-name $TABLE_NAME --region eu-central-1

# Save the table name to a file
echo $TABLE_NAME > data/3-table-name.txt
echo "DynamoDB Table created: $TABLE_NAME"