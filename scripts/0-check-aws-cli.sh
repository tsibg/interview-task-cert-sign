#!/bin/bash
# Check if the AWS CLI is installed and has access to the S3 buckets

# Check the version of the AWS CLI
aws --version
# Check if the AWS CLI has access to the S3 bucket
aws s3 ls --profile=default
