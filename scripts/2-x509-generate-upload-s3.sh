#!/bin/bash

# Generate a certificate and upload it to S3

#Generate a certificate
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout data/2-privatekey.pem -out data/2-certificate.pem -nodes -subj "/C=BG/ST=Sofia/L=Sofia/O=TSI/CN=tsibg.com"

echo "Certificate generated"

#Upload the certificate to S3
BUCKET_NAME=$(cat data/1-bucket-name.txt)
CERT_NAME="certificates/2-certificate.pem"
echo $CERT_NAME > data/2-certificate-name.txt
aws s3 cp data/2-certificate.pem s3://$BUCKET_NAME/$CERT_NAME

echo "Certificate uploaded to S3"
